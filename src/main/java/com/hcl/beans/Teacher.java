package com.hcl.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity(name="teacher")
@Data
@NoArgsConstructor
public class Teacher {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int teacherid;
	private String tname;
	private String temail;
	
	@ToString.Exclude
	@OneToMany(mappedBy = "teacher" ,cascade= CascadeType.ALL)
	@JsonIgnore
	private List<Course> course;
}
