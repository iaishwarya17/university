package com.hcl.beans;

import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/*
 One to Many
@Entity
@Data
@NoArgsConstructor
public class Student {
	@Id
	@GeneratedValue
	private int stdid;
	private String stdname;

	@ToString.Exclude
	@OneToMany(mappedBy="student",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Course> course ;
}*/

@Entity(name="student")
@Data
@NoArgsConstructor
public class Student {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int stdid;
	private String stdname;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "student_courses",
		joinColumns = { @JoinColumn(name = "student_id") },
		inverseJoinColumns = { @JoinColumn (name = "course_id")}
	)
	private List<Course> course;

}