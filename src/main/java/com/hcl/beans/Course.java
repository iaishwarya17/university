package com.hcl.beans;

import java.util.List;




import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/* One to Many
@Entity
@Data
@NoArgsConstructor
public class Course {

	@Id 
	@GeneratedValue
	private int Id;
	private String subject;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="std_id")
	@JsonIgnore
	private Student student;
}*/

@Entity(name="course")
@Data
@NoArgsConstructor
public class Course {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cid;
	private String cname;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="teacher_id")
	private Teacher teacher;

	@ManyToMany(mappedBy = "course", cascade = CascadeType.ALL)
	@JsonIgnore
    private List<Student> student;
	
}
