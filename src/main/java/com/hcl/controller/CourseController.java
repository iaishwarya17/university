package com.hcl.controller;

import java.util.List;



import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.beans.ResponseMessage;
import com.hcl.beans.Course;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;
import com.hcl.service.ICourseService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class CourseController {
	
	@Autowired
	private ICourseService courseService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/addCourse")
	public ResponseEntity<Course> addCourse(@RequestBody Course course) throws UniversityException  {
		
		return new ResponseEntity<Course>(courseService.addCourse(course), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getCourses")
	public ResponseEntity<List<Course>> getCourses() {

		return new ResponseEntity<List<Course>>(courseService.getCourses(), HttpStatus.OK);
	}
	 
    @RequestMapping(method = RequestMethod.POST, value = "/assignTeacher")
	public ResponseEntity<Course> assignTeacher(@RequestBody Course course) throws UniversityException  {
		
		return new ResponseEntity<Course>(courseService.assignTeacher(course), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getCourse/{cid}")
	public ResponseEntity<Course> getCourseById(@PathVariable("cid") Integer cid) throws IdNotFoundException {

		return new ResponseEntity<Course>(courseService.getCourseById(cid), HttpStatus.OK);
	}
	
	@ExceptionHandler(UniversityException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex){
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm,HttpStatus.NOT_FOUND);	
	}

}
