package com.hcl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.beans.Course;
import com.hcl.beans.Teacher;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;
@Service
public interface ITeacherService {

	public Teacher addTeacher(Teacher t) throws UniversityException;
	public List<Teacher> getTeachers();
	public Teacher getTeacherById(Integer teacherid) throws IdNotFoundException;
	public boolean assignCourses(Teacher t,List<Course> c);
	public String deleteTeacher(Integer teacherid) throws IdNotFoundException;

}
