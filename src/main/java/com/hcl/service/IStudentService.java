package com.hcl.service;

import java.util.List;



import com.hcl.beans.Student;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;

public interface IStudentService {

	public Student addStudent(Student std) throws UniversityException;
	public List<Student> getStudents();
	public Student getStudentById(Integer stdid) throws IdNotFoundException;
	public Student chooseCourses(Student student) throws UniversityException;
	public String deleteStudent(Integer stdid) throws IdNotFoundException;
	
}
