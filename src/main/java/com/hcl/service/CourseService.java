package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.beans.Course;
import com.hcl.beans.Teacher;
import com.hcl.dao.ICourseDao;
import com.hcl.dao.IStudentDao;
import com.hcl.dao.ITeacherDao;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;

@Service
public class CourseService implements ICourseService {

	@Autowired
	ICourseDao cdao; 
	@Autowired
	IStudentDao sdao; 
	@Autowired
	ITeacherDao tdao;
	
	@Override
	public Course addCourse(Course course) throws UniversityException {
		// TODO Auto-generated method stub
		return cdao.save(course);
//		List <Student> std = ((Course) course).getStudent();
//	    std.stream().forEach((add)->{add.setCourse(course);});
//		Teacher t = ((Course) course).getTeacher();
//		t.setCourse(course);
//	    cdao.saveAll(course);
//		return (List<Course>) course;
	}

	@Override
	public List<Course> getCourses() {
		// TODO Auto-generated method stub
		System.out.println("inside course service");
		return (List<Course>) cdao.findAll();
	}
	
	@Override
	public Course assignTeacher(Course course) throws UniversityException {
		
		Course c = cdao.findById(course.getCid()).orElseThrow(() -> new UniversityException("id not found"));
		Teacher t = tdao.findById(course.getTeacher().getTeacherid()).orElseThrow(() -> new UniversityException("id not found"));
		c.setTeacher(t);
		return cdao.saveAndFlush(c);
	}

	@Override
	public Course getCourseById(Integer cid) throws IdNotFoundException {
		// TODO Auto-generated method stub
		return cdao.findById(cid).orElseThrow(()-> new IdNotFoundException("course Id not found"));
		
//		Teacher t = tdao.findById(cid).orElseThrow(()->new IdNotFoundException("Id not found"));
//		Course c = (Course) t.getCourse();
//		return c; 

	}

}