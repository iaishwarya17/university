package com.hcl.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.beans.Course;
import com.hcl.beans.Teacher;
import com.hcl.dao.ICourseDao;
import com.hcl.dao.ITeacherDao;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;

@Service
public class TeacherService implements ITeacherService{

	@Autowired
	ITeacherDao tdao; 
	@Autowired
	ICourseDao cdao;
	
	@Override
	public Teacher addTeacher(Teacher t) throws UniversityException {
		// TODO Auto-generated method stub
		Teacher teacher = tdao.findById(t.getTeacherid()).orElseThrow(() -> new UniversityException("Teacher id not found"));
		List <Course> c = cdao.saveAllAndFlush(teacher.getCourse()); //t.getCourse();
        teacher.setCourse(c);
		return teacher;
	//    c.stream().forEach((add)->{add.setTeacher(t);});
	//    tdao.save(t);
	}

	@Override
	public List<Teacher> getTeachers() {
		// TODO Auto-generated method stub
		System.out.println("inside teacher service");
		return (List<Teacher>) tdao.findAll();
	}

	@Override
	public Teacher getTeacherById(Integer teacherid) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Course c = cdao.findById(teacherid).orElseThrow(()->new IdNotFoundException("Id not found"));
		Teacher t = c.getTeacher();
		return t; 
	}

	@Override
	public boolean assignCourses(Teacher t, List<Course> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String deleteTeacher(Integer teacherid) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Teacher t = tdao.findById(teacherid).orElseThrow(()->new IdNotFoundException("Id not found"));
		tdao.delete(t);
		return "Deleted";
	}
}
