package com.hcl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.beans.Course;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;
@Service
public interface ICourseService {

	public Course addCourse(Course course) throws UniversityException;
	public List<Course> getCourses();
	public Course assignTeacher(Course course) throws UniversityException;
	public Course getCourseById(Integer cid) throws IdNotFoundException;
}
