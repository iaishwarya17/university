package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.beans.Course;
import com.hcl.beans.Student;
import com.hcl.dao.ICourseDao;
import com.hcl.dao.IStudentDao;
import com.hcl.exception.IdNotFoundException;
import com.hcl.exception.UniversityException;

@Service
public class StudentService implements IStudentService{

	@Autowired
    IStudentDao sdao;
	@Autowired
    ICourseDao cdao;
	
	@Override
	public Student addStudent(Student std) throws UniversityException{
		// TODO Auto-generated method stub
		return sdao.save(std);
//		List<Course> c = ((Student) std).getCourse();
//	    c.stream().forEach((add)->{add.setStudent(std);});
//	    sdao.saveAll(std); 
//		return std;
	}

	@Override
	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		System.out.println("inside student service");
		return (List<Student>) sdao.findAll();
	}

	@Override
	public Student getStudentById(Integer stdid) throws IdNotFoundException {
		// TODO Auto-generated method stub
		return sdao.findById(stdid).orElseThrow(()-> new IdNotFoundException("student Id not found"));
	}

	@Override
	public Student chooseCourses(Student student) throws UniversityException { 
		// TODO Auto-generated method stub
		Student std = sdao.findById(student.getStdid()).orElseThrow(()-> new UniversityException("student Id not found"));
		List<Course> newList = student.getCourse(); 
		List<Course> existingList = std.getCourse();
		for(Course c : newList){
			Course record = cdao.findById(c.getCid()).orElseThrow(() -> new UniversityException("id not found"));
				existingList.add(record);
		}
		std.setCourse(existingList);
		return sdao.saveAndFlush(std);

	}


	@Override
	public String deleteStudent(Integer stdid) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Student std = sdao.findById(stdid).orElseThrow(()->new IdNotFoundException("Id not found"));
		sdao.delete(std);
		return "Deleted";
	}

}
	
//	@Override
//	public List<Student> getStudentById(Integer id) throws IdNotFoundException{
//		// TODO Auto-generated method stub
////			Student std = dao1.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
////			return dao1.save(std);
//	Course std1 = dao2.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
//	List<Student> stdid = std1.getStudent();
//	return stdid; 
//	}
//	
//	@Override
//	public List<Course> getCourseById(Integer id) throws IdNotFoundException{
//		// TODO Auto-generated method stub
//		Student std = dao1.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
//		List<Course> adrid=std.getCourse();
//		return adrid;
//	}

